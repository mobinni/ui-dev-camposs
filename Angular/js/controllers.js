'use strict';

/* Controllers */

angular.module('camposs.controllers', []).
    controller('HomeController', ['$http', '$scope', '$timeout', function ($http, $scope, $timeout) {
        $http.get('json/campussen.json')
            .success(function (data) {
                $scope.items = data;
                $scope.currentCampus = $scope.items[0];
            });

        <!-- set watch on jquery so digest is called to use the code -->
        $scope.$watch(function () {
            $('.flexslider').flexslider({
                animation: "slide",
                animationLoop: false,
                itemWidth: 210,
                itemMargin: 5,
                controlsContainer: ".flex-container"
            });
        });

        $scope.changeCurrentCampus = function (nr) {
            $scope.currentCampus = $scope.items[nr];
        };
    }
    ])
    .controller('CampusListController', ['$scope', '$routeParams', 'dataFetcher', '$location', function ($scope, $routeParams, dataFetcher, $location) {
        $scope.campusId = $routeParams.campusId;
        $scope.urlParam = $routeParams.name;
        $scope.currentCampus = "Campus " + $scope.urlParam.charAt(0).toUpperCase() + $scope.urlParam.slice(1);
        if ($routeParams.floor >= 0) {
            $scope.floor = $routeParams.floor;
        } else $scope.floor = 0;
        dataFetcher.getCampusData($scope.campusId).then(function (data) {
            $scope.floors = data;
        });

        $scope.filterFloor = (function (nr) {
            $scope.floor = nr - 1;
        });

        $scope.goToVisual = (function () {
            $location.path('/campus/' + $scope.urlParam + '/' + $scope.campusId); // path not hash
        });

        $scope.checkFloor = (function (f) {
            return f === $scope.floor + 1;
        });
    }])
    .controller('CampusVisualController', ['$scope', '$routeParams', 'dataFetcher', '$location', function ($scope, $routeParams, dataFetcher, $location) {
        $scope.campusId = $routeParams.campusId;
        $scope.urlParam = $routeParams.name;
        $scope.currentCampus = "Campus " + $scope.urlParam.charAt(0).toUpperCase() + $scope.urlParam.slice(1);
        if ($routeParams.floor >= 0) {
            $scope.floor = $routeParams.floor;
        } else $scope.floor = 0;


        $scope.drawFloor = (function () {
            $("canvas").clearCanvas();
            var color = '';
            for (var i = 0; i < $scope.floors[$scope.floor].facilities.length; i++) {
                var facility = $scope.floors[$scope.floor].facilities[i];

                if (facility.type === "class") color = "#F7C979";
                if (facility.type === "aula") color = "#F57D49";
                if (facility.type === "cafeteria") color = "#61CEF2";
                if (facility.type === "other") color = "#CCBFAB";
                if (facility.type === "bureau") color = "#75B8BD";
                if (facility.type === "meeting") color = "#D6AEEB";
                if (facility.type === "study landscape") color = "#C9856F";
                if (facility.type === "sanitary") color = "#44A4D4";

                drawRoom(facility, color);
            }
        });

        function drawRoom(facility, color) {
            var canvas = $('#canvas');
            canvas.drawRect({
                layer: true,
                fillStyle: color,
                x: facility.x, y: facility.y,
                width: facility.width,
                height: facility.height,
                fromCenter: false,
                click: function () {
                    document.location.href = '#/campus/' + $scope.urlParam + '/' + $scope.campusId + '/' + $scope.floor + '/' + facility.name;
                }
            });

            canvas.drawText({
                layer: true,
                fillStyle: "#FFFFFF",
                strokeWidth: 2,
                fromCenter: true,
                x: facility.x + (facility.width / 2), y: facility.y + (facility.height / 2),
                fontSize: 20,
                fontFamily: "Segoe UI, Helvetica",
                text: facility.name,
                click: function () {
                    document.location.href = '#/campus/' + $scope.urlParam + '/' + $scope.campusId + '/' + $scope.floor + '/' + facility.name;
                }
            });

            drawBeamers($scope.beamerchecked, facility);
            drawType($scope.typechecked, facility);
            drawSurfaceValues($scope.surfacechecked, facility);
            drawCapacityValues($scope.capacitychecked, facility);
        }

        $scope.filterFloor = (function (nr) {
            $scope.floor = nr - 1;
            $("#canvas").clearCanvas();
            $scope.drawFloor($scope.floors);
        });


        $scope.goToList = (function () {
            $location.path('/campus_list/' + $scope.urlParam + '/' + $scope.campusId); // path not hash
        });

        $scope.checkFloor = (function (f) {
            return f === $scope.floor + 1;
        });

    }])

    .controller('DetailController', ['$scope', '$routeParams', '$location', 'dataFetcher','$http', function ($scope, $routeParams, $location, dataFetcher, $http) {

        $scope.campusId = $routeParams.campusId;
        $scope.urlParam = $routeParams.name;
        $scope.floor = $routeParams.floor;
        $scope.facilityName = $routeParams.facility;
        $scope.base = $location.url().split("/")[1];
        $scope.returnRoute = '#/' + $scope.base + '/' + $scope.urlParam + '/' + $scope.campusId + '/' + $scope.floor;
        $scope.currentRoute = $scope.base + '/' + $scope.urlParam + '/' + $scope.campusId + '/' + $scope.floor + '/' + $scope.facilityName;
        $scope.types = ['class', 'aula', 'meeting', 'study landscape', 'cafeteria' ,'sanitary', 'bureau' ,'other'];

        dataFetcher.getCampusData($scope.campusId).then(function (data) {
            $scope.floors = data;
            for (var i = 0; i < $scope.floors[$scope.floor].facilities.length; i++) {
                var currentFacility = $scope.floors[$scope.floor].facilities[i];
                if (currentFacility.name == $scope.facilityName) {
                    $scope.facility = currentFacility;
                    $scope.dateBulb = $scope.getDateFromInt($scope.facility.dateLastChangedBulb);
                    break;
                } else $scope.facility = data[$scope.floor].facilities[0];
            }
        });

        $scope.getDateFromInt = (function (i) {
            return  new Date(parseInt(i)).toLocaleDateString();
        });

        // HTTP POST METHOD
        $scope.saveFacility = (function(facility){
            $scope.facility = facility;
            $http({
                method : 'POST',
                url : $scope.currentRoute + '/post',
                data : angular.toJson($scope.facility),
                headers : {
                    'Content-Type' : 'application/x-www-form-urlencoded'
                }
            });
        });

    }])

    .controller('PostFacilityController', ['$scope', '$routeParams', '$location', 'dataFetcher','$http', function ($scope, $routeParams, $location, dataFetcher, $http) {

        $scope.campusId = $routeParams.campusId;
        $scope.urlParam = $routeParams.name;
        $scope.floor = $routeParams.floor;
        $scope.facilityName = $routeParams.facility;
        $scope.base = $location.url().split("/")[1];
        $scope.returnRoute = '#/' + $scope.base + '/' + $scope.urlParam + '/' + $scope.campusId + '/' + $scope.floor;
        $scope.currentRoute = $scope.base + '/' + $scope.urlParam + '/' + $scope.campusId + '/' + $scope.floor + '/' + $scope.facilityName;

        alert('gay');


    }])

  .controller('ReportController', ['$scope', '$location', '$rootScope', function ($scope, $location, $root) {

    }])

    .controller('MenuController', ['$scope', '$location', '$rootScope', function ($scope, $location, $root) {
        $scope.homeId = '';
        $scope.campusId = '';
        $root.$on("$routeChangeStart", function (event, next, current) {
            if ($location.path() == '/home') {
                $scope.homeId = 'current';
                $scope.campusId = '';
            }
            else {
                $scope.campusId = 'current';
                $scope.homeId = '';
            }
        });

    }]);


function drawBeamers(isChecked, facility) {
    var canvas = $('#canvas');
    if (isChecked && facility.hasBeamer) {
        var scale = 0.5;
        var offSetY = 15;
        var offSetX = 35;
        if (facility.width < 250) {
            scale = 0.3;
            offSetX = 20;
            offSetY = 10;
        }
        canvas.drawImage({
            layer: true,
            source: "images/projector.png",
            fromCenter: true,
            x: facility.x + offSetX, y: facility.y + offSetY,
            scale: scale,
            click: function () {
                document.location.href = '#/campus/' + $scope.urlParam + '/' + $scope.campusId + '/' + $scope.floor + '/' + facility.name;
            }
        });
    }
}

function drawType(isChecked, facility) {
    var canvas = $('#canvas');
    if (isChecked) {
        var scale = 0.2;
        var image = "images/type_class.png";
        if (facility.width < 250) {
            scale = 0.1;
        }
        var offSetY = facility.height - (scale * 300);
        var offSetX = facility.width - (scale * 300);

        if (facility.type === "aula") image = "images/type_aula.png";
        if (facility.type === "cafeteria")  image = "images/type_cafeteria.png";
        if (facility.type === "study landscape")  image = "images/type_study.png";
        if (facility.type === "sanitary")  image = "images/type_toilet.png";
        if (facility.type === "meeting")  image = "images/type_meeting.png";
        if (facility.type === "other")  image = "images/type_other.png";
        if (facility.type === "bureau")  image = "images/type_bureau.png";

        canvas.drawImage({
            layer: true,
            source: image,
            fromCenter: true,
            x: facility.x + offSetX, y: facility.y + offSetY,
            scale: scale,
            opacity: 0.8,
            click: function () {
                document.location.href = '#/campus/' + $scope.urlParam + '/' + $scope.campusId + '/' + $scope.floor + '/' + facility.name;
            }
        });
    }
}

function drawSurfaceValues(isChecked, facility) {
    var canvas = $('#canvas');
    if (isChecked) {
        canvas.drawText({
            layer: true,
            fillStyle: "#FFFFFF",
            strokeWidth: 2,
            fromCenter: true,
            x: facility.x + (facility.width / 2), y: facility.y + (facility.height / 2) + 25,
            fontSize: 20,
            fontFamily: "Segoe UI, Helvetica",
            text: facility.surface + "m²",
            click: function () {
                document.location.href = '#/campus/' + $scope.urlParam + '/' + $scope.campusId + '/' + $scope.floor + '/' + facility.name;
            }
        });
        canvas.drawImage({
            layer: true,
            source: "images/surface.png",
            fromCenter: true,
            x: facility.x + (facility.width / 2) - 40, y: facility.y + (facility.height / 2) + 25,
            scale: 0.07,
            opacity: 0.8,
            click: function () {
                document.location.href = '#/campus/' + $scope.urlParam + '/' + $scope.campusId + '/' + $scope.floor + '/' + facility.name;
            }
        });
    }
}

function drawCapacityValues(isChecked, facility) {
    var canvas = $('#canvas');
    if (isChecked) {
        canvas.drawText({
            layer: true,
            fillStyle: "#FFFFFF",
            strokeWidth: 2,
            fromCenter: true,
            x: facility.x + (facility.width / 2), y: facility.y + 20,
            fontSize: 20,
            fontFamily: "Segoe UI, Helvetica",
            text: facility.capacity
        });
        canvas.drawImage({
            layer: true,
            source: "images/capacity.png",
            fromCenter: true,
            x: facility.x + (facility.width / 2) + 30, y: facility.y + 20,
            scale: 0.5,
            opacity: 0.75
        });
    }
}
