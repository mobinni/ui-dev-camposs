/**
 * Created by M on 3/12/13.
 */

function showTime()
{
    var today=new Date();
    // add a zero in front of numbers<10
    $("#clock").text(today.getUTCDate() + "/" + (today.getMonth() + 1) + "/" + today.getFullYear() + " - " + checkTime(today.getHours())+ ":" + checkTime(today.getMinutes()) + ":" + checkTime(today.getSeconds()));
    t=setTimeout('showTime()',1000);
}
function checkTime(i)
{
    if (i<10)
    {
        i="0" + i;
    }
    return i;
}
showTime();

$( document ).ready(function() {
    $('#myOnOff').click(function() {
        if( $('#myOnOff').is(':checked')) {
            $("#screen").attr("href", "css/light/screen.css");
            $("#flex").attr("href", "css/light/flexslider.css");
            $(".back").attr("src", "images/back.png");
        } else {
            $("#screen").attr("href", "css/dark/screen.css");
            $("#flex").attr("href", "css/dark/flexslider.css");
            $(".back").attr("src", "images/back_black.png");
        }

    });

});