'use strict';

/* Directives */


angular.module('camposs.directives', []).
    directive('appVersion', ['version', function (version) {
        return function (scope, elm, attrs) {
            elm.text(version);
        };
    }])
    .directive('floorplan', ['dataFetcher', function (dataFetcher) {
        return {
            restrict: 'EA',
            replace: true,
            transclude: true,
            link: function(scope, element, attrs) {
                dataFetcher.getCampusData(scope.campusId).then(function (data) {
                    scope.floors = data;
                    scope.drawFloor(scope.floors);
                });
            }
        }
    }]);
