'use strict';


// Declare app level module which depends on filters, and services
angular.module('camposs', [
        'ngRoute',
        'camposs.filters',
        'camposs.services',
        'camposs.directives',
        'camposs.controllers'
    ]).
    config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/home', {templateUrl: 'partials/home.html', controller: 'HomeController'});

        $routeProvider.when('/campus/:name/:campusId', {templateUrl: 'partials/campus.html', controller: 'CampusVisualController'});
        $routeProvider.when('/campus/:name/:campusId/:floor', {templateUrl: 'partials/campus.html', controller: 'CampusVisualController'});
        $routeProvider.when('/campus/:name/:campusId/:floor/:facility', {templateUrl: 'partials/facility.html', controller: 'DetailController'});
        $routeProvider.when('/campus/:name/:campusId/:floor/:facility/edit', {templateUrl: 'partials/facility_edit.html', controller: 'DetailController'});
        $routeProvider.when('/campus/:name/:campusId/:floor/:facility/report', {templateUrl: 'partials/report.html', controller: 'ReportController'});

        $routeProvider.when('/campus_list/:name/:campusId', {templateUrl: 'partials/campus_list.html', controller: 'CampusListController'});
        $routeProvider.when('/campus_list/:name/:campusId/:floor', {templateUrl: 'partials/campus_list.html', controller: 'CampusListController'});
        $routeProvider.when('/campus_list/:name/:campusId/:floor/:facility', {templateUrl: 'partials/facility.html', controller: 'DetailController'});
        $routeProvider.when('/campus_list/:name/:campusId/:floor/:facility/edit', {templateUrl: 'partials/facility_edit.html', controller: 'DetailController'});
        $routeProvider.when('/campus_list/:name/:campusId/:floor/:facility/report', {templateUrl: 'partials/report.html', controller: 'ReportController'});

        $routeProvider.otherwise({redirectTo: '/home'});
    }]);
