/**
 * Created by M on 3/12/13.
 */
$( document ).ready(function() {
    $('#myOnOff').click(function() {
        if( $('#myOnOff').is(':checked')) {
            $("#screen").attr("href", "css/light/screen.css");
            $("#flex").attr("href", "css/light/flexslider.css");
        } else {
            $("#screen").attr("href", "css/dark/screen.css");
            $("#flex").attr("href", "css/dark/flexslider.css");
        }

    });

});