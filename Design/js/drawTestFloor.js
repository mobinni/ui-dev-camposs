var canvas = $("canvas");


// GR101
canvas.drawRect({
    layer: true,
    fillStyle: "#138F34",
    x: 100, y: 65,
    width: 200,
    height: 130,
    opacity: 0.8,
    click: function() {
        document.location.href="facility.html";
    }
});
canvas.drawText({
    layer: true,
    fillStyle: "#FFFFFF",
    strokeWidth: 2,
    x: 30, y: 15,
    fontSize: 20,
    fontFamily: "Segoe UI, Helvetica",
    text: "GR101"
});
canvas.drawImage({
    source: "images/projector.png",
    x: 170, y: 20,
    scale: 0.5
});
// GR102
canvas.drawRect({
    layer: true,
    fillStyle: "#138F34",
    x: 310, y: 65,
    width: 200,
    height: 130,
    opacity: 0.8,
    click: function() {
        document.location.href="facility.html";
    }
});
canvas.drawText({
    layer: true,
    fillStyle: "#FFFFFF",
    strokeWidth: 2,
    x: 240, y: 15,
    fontSize: 20,
    fontFamily: "Segoe UI, Helvetica",
    text: "GR102"
});

canvas.drawImage({
    source: "images/projector.png",
    x: 370, y: 20,
    scale: 0.5
});


// GR103
canvas.drawRect({
    layer: true,
    fillStyle: "#138F34",
    x: 520, y: 65,
    width: 200,
    height: 130,
    opacity: 0.8,
    click: function() {
        document.location.href="facility.html";
    }
});
canvas.drawText({
    layer: true,
    fillStyle: "#FFFFFF",
    strokeWidth: 2,
    x: 450, y: 15,
    fontSize: 20,
    fontFamily: "Segoe UI, Helvetica",
    text: "GR103"
});
canvas.drawImage({
    source: "images/projector.png",
    x: 590, y: 20,
    scale: 0.5
});
// GR104
canvas.drawRect({
    layer: true,
    fillStyle: "#138F34",
    x: 730, y: 65,
    width: 200,
    height: 130,
    opacity: 0.8,
    click: function() {
        document.location.href="facility.html";
    }
});
canvas.drawText({
    layer: true,
    fillStyle: "#FFFFFF",
    strokeWidth: 2,
    x: 660, y: 15,
    fontSize: 20,
    fontFamily: "Segoe UI, Helvetica",
    text: "GR104"
});
// GR105
canvas.drawRect({
    layer: true,
    fillStyle: "#138F34",
    x: 930, y: 65,
    width: 150,
    height: 130,
    opacity: 0.8,
    click: function() {
        document.location.href="facility.html";
    }
});
canvas.drawText({
    layer: true,
    fillStyle: "#FFFFFF",
    strokeWidth: 2,
    x: 885, y: 15,
    fontSize: 20,
    fontFamily: "Segoe UI, Helvetica",
    text: "GR105"
});
// GR106
canvas.drawRect({
    layer: true,
    fillStyle: "#138F34",
    x: 70, y: 245,
    width: 150,
    height: 200,
    opacity: 0.8,
    click: function() {
        document.location.href="facility.html";
    }
});
canvas.drawText({
    layer: true,
    fillStyle: "#FFFFFF",
    strokeWidth: 2,
    x: 30, y: 160,
    fontSize: 20,
    fontFamily: "Segoe UI, Helvetica",
    text: "GR106"
});
// GR107
canvas.drawRect({
    layer: true,
    fillStyle: "#E89D1C",
    x: 70, y: 470,
    width: 150,
    height: 240,
    opacity: 0.8,
    click: function() {
        document.location.href="facility.html";
    }
});
canvas.drawText({
    layer: true,
    fillStyle: "#FFFFFF",
    strokeWidth: 2,
    x: 30, y: 370,
    fontSize: 20,
    fontFamily: "Segoe UI, Helvetica",
    text: "GR107"
});
// GR108
canvas.drawRect({
    layer: true,
    fillStyle: "#138F34",
    x: 70, y: 695,
    width: 150,
    height: 200,
    opacity: 0.8,
    click: function() {
        document.location.href="facility.html";
    }
});
canvas.drawText({
    layer: true,
    fillStyle: "#FFFFFF",
    strokeWidth: 2,
    x: 30, y: 610,
    fontSize: 20,
    fontFamily: "Segoe UI, Helvetica",
    text: "GR108"
});

// GR109
canvas.drawRect({
    layer: true,
    fillStyle: "#138F34",
    x: 290, y: 735,
    width: 250,
    height: 130,
    opacity: 0.8,
    click: function() {
        document.location.href="facility.html";
    }
});
canvas.drawText({
    layer: true,
    fillStyle: "#FFFFFF",
    strokeWidth: 2,
    x: 200, y: 685,
    fontSize: 20,
    fontFamily: "Segoe UI, Helvetica",
    text: "GR109"
});
// GR110
canvas.drawRect({
    layer: true,
    fillStyle: "#138F34",
    x: 560, y: 735,
    width: 250,
    height: 130,
    opacity: 0.8,
    click: function() {
        document.location.href="facility.html";
    }
});
canvas.drawText({
    layer: true,
    fillStyle: "#FFFFFF",
    strokeWidth: 2,
    x: 470, y: 685,
    fontSize: 20,
    fontFamily: "Segoe UI, Helvetica",
    text: "GR110"
});
// GR111
canvas.drawRect({
    layer: true,
    fillStyle: "#9C1124",
    x: 850, y: 695,
    width: 300,
    height: 300,
    opacity: 0.8,
    click: function() {
        document.location.href="facility.html";
    }
});
canvas.drawText({
    layer: true,
    fillStyle: "#FFFFFF",
    strokeWidth: 2,
    x: 740, y: 560,
    fontSize: 20,
    fontFamily: "Segoe UI, Helvetica",
    text: "GR111"
});


