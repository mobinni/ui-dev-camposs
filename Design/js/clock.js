function showTime()
{
    var today=new Date();
    // add a zero in front of numbers<10
    $("#clock").text(today.getUTCDate() + "/" + (today.getMonth() + 1) + "/" + today.getFullYear() + " - " + checkTime(today.getHours())+ ":" + checkTime(today.getMinutes()) + ":" + checkTime(today.getSeconds()));
    t=setTimeout('showTime()',1000);
}
function checkTime(i)
{
    if (i<10)
    {
        i="0" + i;
    }
    return i;
}
showTime();